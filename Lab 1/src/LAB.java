import java.util.Scanner;
public class LAB {
    public static void main(String[] args){
        Scanner myObj = new Scanner(System.in);
        System.out.println("Menu :");
        System.out.println("Menu 1 : Print ITE");
        System.out.println("Menu 2 : Print Engineering");
        System.out.println("Menu 3 : Print Table");
        System.out.print("Input Number of Menu : ");
        int menu = myObj.nextInt();
        System.out.println("---------------------------------");
        switch (menu) {
            case 1:
                System.out.println("        ***         ***         ***********       **********      **********");
                System.out.println("       *****       *****        ***********       **********      **********");
                System.out.println("      *******************           ***               ***         ****      ");
                System.out.println("       *****************            ***               ***         ****      ");
                System.out.println("        ***************             ***               ***         **********");
                System.out.println("         *************              ***               ***         **********");
                System.out.println("          ***********               ***               ***         ****      ");
                System.out.println("           *********                ***               ***         ****      ");
                System.out.println("             ******              **********        *********      **********");
                System.out.println("              ***                **********        *********      **********");
            break;
            case 2:
                System.out.println("        ***              *              **                          *                                                                      ");
                System.out.println("       ***              * *            *  *                         ***                                                                 ");
                System.out.println("      ***             *   *             **                          *  *                                                                ");
                System.out.println("    ***              *    *                                         *****                                                               ");
                System.out.println("    ***             *********                                                                                                       ");
                System.out.println("    ***                   *         **********                         **         *****            ****        *********       ****    ");
                System.out.println("    ***                             **      **                         **         **               **                          **      ");
                System.out.println("    ***                             ***     **                         **      **********           **         *********       **      ");
                System.out.println("    ***                                     **     ****             *****      **      **           **         **     **       ******  ");
                System.out.println("    ***                             **********                      **         **      **           **         **     **           **  ");
                System.out.println("    ***                             **                              **         **      **           **         **     **           **  ");
                System.out.println("    ***                             **                              *****      **      **           **         **     **           **  ");
                System.out.println("    ***                             **********     ****                **      **      **           **         **     **           **  ");
                System.out.println("    ***                                                                **      **      **           **         **     **           **  ");
                System.out.println("    ***                              *     ****                        **      ******  **           **         **     **           **  ");
                System.out.println("    ***                                **   **                      ** **      **  **  **           **         **     **        *****  ");
                System.out.println("    ***                                   **                        ** **      ******  ***************         **     **        *****  ");
                System.out.println("    ***                                                                                                                                 ");
                System.out.println("    ***            *****                  ***                                      ***********                                     ");
                System.out.println("    ***               **                   **                                      ***    **                                           ");
                System.out.println("    ***               **                    *                                      ***  **                                                     ");
                System.out.println("   ***                **                                                           ****");
                System.out.println("  ***              ******                                                          **");
                System.out.println(" ***  ");
                System.out.println("***");
                break;
            case 3:
                System.out.println("+=======================================================================+");
                System.out.println("|   ID      Name           Gender       Address            Phone        |");
                System.out.println("+=======================================================================+");
                System.out.println("|   1       Chan Vichet     M           Phnom Penh         018 373485   |");
                System.out.println("|   2       Chey Dara       M           Kankal             017 348384   |");
                System.out.println("|   3       Thy Dalin       F           Prey Veng          012 909047   |");
                System.out.println("|   4       Nuon Dary       F           Prey Veng          012 908070   |");
                System.out.println("|   5       Mey Daly        M           Kompong Cham       012 994367   |");
                System.out.println("|   6       Mo Malissa      F           Kompong Cham       012 234864   |");
                System.out.println("|   7       Muon Darong     M           Kompong Cham       012 884367   |");
                System.out.println("+-----------------------------------------------------------------------+");
                break;
            default:
                System.out.println("Input Wrong Number");
                break;
        }
    }
}
